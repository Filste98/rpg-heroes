# RPG HEROES


## Background
This application consists of a program that can create Heroes with a set of classes. Based on the class the hero will have different properties. A hero can level up, equip armor and weapons, calculate their damage and display their information. The application has no userinterface but is unit-tested.

### Level up
Based on the class the heroes' attributes and level will increase. The attributes are as follows: Strength, Dexterity and Intelligence.

### Equip
Heroes can equip armor and weapons. The hero will need a required level to equip certain equipment. The heroes can also only equip weapons and armor that corresponds to their class.

### Calculate damage
The heroes' will have a damageattribute that is calculated based on their heroattributes and equipment. The damagecalculation will vary between classes, as mages use intelligence, warriors use strength, and rogues/rangers use dexterity.

### Display
Returns a stringbuilder with an overview of the heroes' level, attribute, class, name and damage.


## Usage
Open i Visual Studio. Navigate to the UnitTest project. Right-click the test you want to execute and press "Run Tests". You can optionally use "CTRL+R, T"  on Windows.


## Authors
Gitlab: Filste98
Gitlab link: https://gitlab.com/Filste98


## License
[MIT](https://choosealicense.com/licenses/mit/)
