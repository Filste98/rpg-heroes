﻿using RPG_Heroes.Equipables;
using RPG_Heroes.Hero_Types;
using RPG_Heroes.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RPG_Heroes
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public HeroAttributes LevelAttributes { get; init; }
        public HeroAttributes heroAttributes { get; init; }
        public Dictionary<Slot, Equipable> Equipment { get; set; }
        public string[] ValidWeaponTypes { get; init; }
        public string[] ValidArmorTypes { get; init; }

        public Hero(string name, string[] validWeaponTypes, string[] validArmorTypes, HeroAttributes heroAttributes, HeroAttributes levelAttributes)
        {
            Name = name;
            ValidWeaponTypes = validWeaponTypes;
            ValidArmorTypes = validArmorTypes;
            Equipment = generateSlot();
            this.heroAttributes = heroAttributes;
            LevelAttributes = levelAttributes;
        }

        private Dictionary<Slot, Equipable> generateSlot()
        {
            Dictionary<Slot, Equipable> result = new Dictionary<Slot, Equipable>();

            foreach (Slot slot in Enum.GetValues(typeof(Slot)))
            {
                result.Add(slot, null!);
            }

            return result;
        }

        public virtual void LevelUp()
        {
            Level++;
        }

        public void Equip(Armor armor)
        {
            //Checks if Correct class for armortype:
            if (!ValidArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException("InvalidArmorException");
            }

            //Checks if high enough level to equip:
            if (Level < armor.RequiredLevel)
            {
                throw new InvalidArmorException("InvalidArmorException");
            }

            Equipment[armor.slot] = armor;
        }

        public void Equip(Weapon weapon)
        { 
            //Checks if Correct class for weapontype:
            if (!ValidWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException("InvalidWeaponException");
            }

            //Checks if high enough level to equip:
            if(Level < weapon.RequiredLevel)
            {
                throw new InvalidWeaponException("InvalidWeaponException");
            }

            Equipment[Slot.Weapon] = weapon;
        }

        public double Damage()
        {
            double weaponDamage = 0;
            try
            {
                Weapon weapon = (Weapon)Equipment[Slot.Weapon];
                weaponDamage = weapon.WeaponDamage;
            }
            catch
            {
                weaponDamage = 1;
            }

            switch(this.GetType().Name)
            {
                case "Mage":
                    return weaponDamage * (1 + (double)heroAttributes.intelligence / 100);
                case "Warrior":
                    return weaponDamage * (1 + (double)heroAttributes.strength / 100);
                default:
                    return weaponDamage * (1 + (double)heroAttributes.dexterity / 100);
            }
        }

        public HeroAttributes TotalAttributes()
        {
            HeroAttributes TotalAttributes = heroAttributes;

            //TODO Make this a for-loop...

            if (Equipment[Slot.Head] != null)
            {
                Armor helm = (Armor)Equipment[Slot.Head];
                TotalAttributes.strength += helm.ArmorAttributes.strength;
                TotalAttributes.dexterity += helm.ArmorAttributes.dexterity;
                TotalAttributes.intelligence += helm.ArmorAttributes.intelligence;
            }

            if (Equipment[Slot.Body] != null)
            {
                Armor body = (Armor)Equipment[Slot.Body];
                TotalAttributes.strength += body.ArmorAttributes.strength;
                TotalAttributes.dexterity += body.ArmorAttributes.dexterity;
                TotalAttributes.intelligence += body.ArmorAttributes.intelligence;
            }

            if (Equipment[Slot.Legs] != null)
            {
                Armor legs = (Armor)Equipment[Slot.Legs];
                TotalAttributes.strength += legs.ArmorAttributes.strength;
                TotalAttributes.dexterity += legs.ArmorAttributes.dexterity;
                TotalAttributes.intelligence += legs.ArmorAttributes.intelligence;
            }
            
            return TotalAttributes;
        }

        public StringBuilder Display()
        {
            HeroAttributes totalAttributes = TotalAttributes();

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("-------------------------------");
            sb.AppendLine("Name:\t\t\t" + Name);
            sb.AppendLine("Class:\t\t\t" + this.GetType().Name);
            sb.AppendLine("Level:\t\t\t" + Level);
            sb.AppendLine("Total strength:\t\t" + totalAttributes.strength);
            sb.AppendLine("Total dexterity:\t" + totalAttributes.dexterity);
            sb.AppendLine("Total intelligence:\t" + totalAttributes.intelligence);
            sb.AppendLine("Damage:\t\t\t" + Damage());
            sb.AppendLine("-------------------------------");

            return sb;
        }
    }
}
