﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Hero_Types
{
    public class Rogue : Hero
    {
        public string name { get; set; }

        private static HeroAttributes heroAttributes = new HeroAttributes(2, 6, 1);

        private static HeroAttributes RogueLevelAttributes { get; set; } = new HeroAttributes(1, 4, 1);

        private static string[] ValidWeaponTypes = { "Dagger", "Sword" };

        private static string[] ValidArmorTypes = { "Leather", "Mail" };
        public Rogue(string name) : base(name, ValidWeaponTypes, ValidArmorTypes, heroAttributes, RogueLevelAttributes)
        {
        }

        public override void LevelUp()
        {
            base.LevelUp();
            heroAttributes.strength += RogueLevelAttributes.strength;
            heroAttributes.dexterity += RogueLevelAttributes.dexterity;
            heroAttributes.intelligence += RogueLevelAttributes.intelligence;
        }
    }
}
