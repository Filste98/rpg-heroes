﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Hero_Types
{
    public class Mage : Hero
    {
        public string name { get; set; }

        private static HeroAttributes heroAttributes = new HeroAttributes(1,1,8);

        private static HeroAttributes MageLevelAttributes { get; set; } = new HeroAttributes(1, 1, 5);

        private static string[] ValidWeaponTypes = { "Staff", "Wand" };

        private static string[] ValidArmorTypes = { "Cloth" };
        public Mage(string name) : base(name, ValidWeaponTypes, ValidArmorTypes, heroAttributes, MageLevelAttributes) 
        {
        }

        public override void LevelUp()
        {
            base.LevelUp();    
            heroAttributes.strength += MageLevelAttributes.strength;
            heroAttributes.dexterity += MageLevelAttributes.dexterity;
            heroAttributes.intelligence += MageLevelAttributes.intelligence;
        }    
    }
}
