﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Hero_Types
{
    public class Warrior : Hero
    {
        public string name { get; set; }

        private static HeroAttributes heroAttributes = new HeroAttributes(5, 2, 1);

        private static HeroAttributes WarriorLevelAttributes { get; set; } = new HeroAttributes(3, 2, 1);

        private static string[] ValidWeaponTypes = { "Axe", "Hammer", "Sword" };

        private static string[] ValidArmorTypes = { "Mail", "Plate" };
        public Warrior(string name) : base(name, ValidWeaponTypes, ValidArmorTypes, heroAttributes, WarriorLevelAttributes)
        {
        }

        public override void LevelUp()
        {
            base.LevelUp();
            heroAttributes.strength += WarriorLevelAttributes.strength;
            heroAttributes.dexterity += WarriorLevelAttributes.dexterity;
            heroAttributes.intelligence += WarriorLevelAttributes.intelligence;
        }
    }
}
