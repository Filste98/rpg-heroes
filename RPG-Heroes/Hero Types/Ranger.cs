﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Hero_Types
{
    public class Ranger : Hero
    {
        public string name { get; set; }

        private static HeroAttributes heroAttributes = new HeroAttributes(1, 7, 1);

        private static HeroAttributes RangerLevelAttributes { get; set; } = new HeroAttributes(1, 5, 1);

        private static string[] ValidWeaponTypes = { "Bow" };

        private static string[] ValidArmorTypes = { "Leather", "Mail" };
        public Ranger(string name) : base(name, ValidWeaponTypes, ValidArmorTypes, heroAttributes, RangerLevelAttributes)
        {
        }

        public override void LevelUp()
        {
            base.LevelUp();
            heroAttributes.strength += RangerLevelAttributes.strength;
            heroAttributes.dexterity += RangerLevelAttributes.dexterity;
            heroAttributes.intelligence += RangerLevelAttributes.intelligence;
        }
    }
}
