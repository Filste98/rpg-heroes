﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes
{
    public class InvalidArmorException : Exception
    {
        public InvalidArmorException(string msg) : base(msg)
        {

        }
    }
}
