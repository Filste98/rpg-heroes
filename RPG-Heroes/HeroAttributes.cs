﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes
{
    public class HeroAttributes
    {
        public int strength { get; set; }
        public int dexterity { get; set; }
        public int intelligence { get; set; }

        public HeroAttributes(int strength, int dexterity, int intelligence)
        {
            this.strength = strength;
            this.dexterity = dexterity;
            this.intelligence = intelligence;
        }

        /*public HeroAttributes(HeroAttributes levelAttributes)
        {
            strength = levelAttributes.strength;
            dexterity = levelAttributes.dexterity;
            intelligence = levelAttributes.intelligence;
        }*/

 





    }
}
