﻿using RPG_Heroes.Equipables;
using RPG_Heroes.Hero_Types;

namespace RPG_Heroes
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Mage hero = new Mage("Gandalf");
            hero.Display();
        }
    }
}