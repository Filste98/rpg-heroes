﻿using RPG_Heroes.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Equipables
{
    public abstract class Equipable
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public Slot slot { get; set; }

        public Equipable(string name, int requiredLevel, Slot slot)
        {
            Name = name;
            RequiredLevel = requiredLevel;
            this.slot = slot;
        }
    }
}
