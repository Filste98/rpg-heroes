﻿using RPG_Heroes.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Heroes.Equipables
{
    public class Armor : Equipable
    {
        public string ArmorType { get; set; }

        public HeroAttributes ArmorAttributes { get; set; }

        public Armor(string name, int requiredLevel, Slot slot, string armorType, HeroAttributes armorAttributes) : base(name, requiredLevel, slot)
        {
            ArmorType = armorType;
            ArmorAttributes = armorAttributes;
        }

    }
}
