using RPG_Heroes;
using RPG_Heroes.Equipables;
using RPG_Heroes.Hero_Types;
using FluentAssertions;
using System.Reflection.Emit;
using System.Text;
using System.Xml.Linq;

namespace UnitTest
{
    public class UnitTest
    {
        //---------------- MAGE ------------------------------------------------------------------
        [Fact]
        public void MageConstructor_InputsValidName_ShouldCreateMageWithCorrectStarterLevels()
        {
            //Arrange
            Mage mage = new Mage("Jon");
            HeroAttributes expected = new HeroAttributes(1, 1, 8);
            //Act
            HeroAttributes actual = mage.heroAttributes;
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }
        [Fact]
        public void MageConstructor_InputsValidName_ShouldCreateMageWithCorrectLevel()
        {
            //Arrange
            Mage mage = new Mage("Jon");
            int expected = 1;
            //Act
            int actual = mage.Level;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void MageConstructor_InputsValidName_ShouldCreateMageWithSameName()
        {
            //Arrange
            Mage mage = new Mage("Jon");
            string expected = "Jon";
            //Act
            string actual = mage.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void MageLevelUp_ChecksIfLevelIncreases_ShouldIncreaseByOne()
        {
            //Arrange
            Mage hero = new Mage("Jeff");
            int expected = 2;
            //Act
            hero.LevelUp();
            int actual = hero.Level;
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void MageLevelUp_ChecksIfHeroAttributesIncreases_ShouldIncreaseByOneOneFive()
        {
            //Arrange
            Mage hero = new Mage("Jeff");
            int expectedStrength = hero.heroAttributes.strength + 1;
            int expectedDexterity = hero.heroAttributes.dexterity + 1;
            int expectedIntelligence = hero.heroAttributes.intelligence + 5;
            //Act
            hero.LevelUp();
            int actualStrength = hero.heroAttributes.strength;
            int actualDexterity = hero.heroAttributes.dexterity;
            int actualIntelligence = hero.heroAttributes.intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);

        }

        //---------------- RANGER ------------------------------------------------------------------
        [Fact]
        public void RangerConstructor_InputsValidName_ShouldCreateRangerWithCorrectStarterLevels()
        {
            //Arrange
            Ranger ranger = new Ranger("Jon");
            HeroAttributes expected = new HeroAttributes(1, 7, 1);
            //Act
            HeroAttributes actual = ranger.heroAttributes;
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void RangerConstructor_InputsValidName_ShouldCreateRangerWithCorrectLevel()
        {
            //Arrange
            Ranger ranger = new Ranger("Jon");
            int expected = 1;
            //Act
            int actual = ranger.Level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerConstructor_InputsValidName_ShouldCreateRangerWithSameName()
        {
            //Arrange
            Ranger ranger = new Ranger("Jon");
            string expected = "Jon";
            //Act
            string actual = ranger.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RangerLevelUp_ChecksIfLevelIncreases_ShouldIncreaseByOne()
        {
            //Arrange
            Ranger hero = new Ranger("Jeff");
            int expected = 1 + 1;
            //Act
            hero.LevelUp();
            int actual = hero.Level;
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void RangerLevelUp_ChecksIfHeroAttributesIncreases_ShouldIncreaseByOneFiveOne()
        {
            //Arrange
            Ranger hero = new Ranger("Jeff");
            int expectedStrength = hero.heroAttributes.strength + 1;
            int expectedDexterity = hero.heroAttributes.dexterity + 5;
            int expectedIntelligence = hero.heroAttributes.intelligence + 1;
            //Act
            hero.LevelUp();
            int actualStrength = hero.heroAttributes.strength;
            int actualDexterity = hero.heroAttributes.dexterity;
            int actualIntelligence = hero.heroAttributes.intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);

        }

        //---------------- ROGUE ------------------------------------------------------------------
        [Fact]
        public void RogueConstructor_InputsValidName_ShouldCreateRogueWithCorrectStarterLevelse()
        {
            //Arrange
            Rogue rogue = new Rogue("Jon");
            HeroAttributes expected = new HeroAttributes(2, 6, 1);
            //Act
            HeroAttributes actual = rogue.heroAttributes;
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void RogueConstructor_InputsValidName_ShouldCreateRogueWithCorrectLevel()
        {
            //Arrange
            Rogue rogue = new Rogue("Jon");
            int expected = 1;
            //Act
            int actual = rogue.Level;
            //Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void RogueConstructor_InputsValidName_ShouldCreateRogueWithSameName()
        {
            //Arrange
            Rogue rogue = new Rogue("Jon");
            string expected = "Jon";
            //Act
            string actual = rogue.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void RogueLevelUp_ChecksIfLevelIncreases_ShouldIncreaseByOne()
        {
            //Arrange
            Rogue rogue = new Rogue("Jeff");
            int expected = 2;
            //Act
            rogue.LevelUp();
            int actual = rogue.Level;
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void RogueLevelUp_ChecksIfHeroAttributesIncreases_ShouldIncreaseByOneFourOne()
        {
            //Arrange
            Rogue hero = new Rogue("Jeff");
            int expectedStrength = hero.heroAttributes.strength + 1;
            int expectedDexterity = hero.heroAttributes.dexterity + 4;
            int expectedIntelligence = hero.heroAttributes.intelligence + 1;
            //Act
            hero.LevelUp();
            int actualStrength = hero.heroAttributes.strength;
            int actualDexterity = hero.heroAttributes.dexterity;
            int actualIntelligence = hero.heroAttributes.intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);

        }

        //---------------- WARRIOR ------------------------------------------------------------------
        [Fact]
        public void WarriorConstructor_InputsValidName_ShouldCreateWarriorWithCorrectStarterLevels()
        {
            //Arrange
            Warrior warrior = new Warrior("Jon");
            HeroAttributes expected = new HeroAttributes(5, 2, 1);
            //Act
            HeroAttributes actual = warrior.heroAttributes;
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public void WarriorConstructor_InputsValidName_ShouldCreateWarriorWithCorrectLevel()
        {
            //Arrange
            Warrior warrior = new Warrior("Jon");
            int expected = 1;
            //Act
            int actual = warrior.Level;
            //Assert
            Assert.Equal(expected, actual);
        }


        [Fact]
        public void WarriorConstructor_InputsValidName_ShouldCreateWarriorWithSameName()
        {
            //Arrange
            Warrior warrior = new Warrior("Jon");
            string expected = "Jon";
            //Act
            string actual = warrior.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WarriorLevelUp_ChecksIfLevelIncreases_ShouldIncreaseByOne()
        {
            //Arrange
            Warrior warrior = new Warrior("Jeff");
            int expected = 2;
            //Act
            warrior.LevelUp();
            int actual = warrior.Level;
            //Assert
            Assert.Equal(expected, actual);

        }

        [Fact]
        public void WarriorLevelUp_ChecksIfHeroAttributesIncreases_ShouldIncreaseByThreeTwoOne()
        {
            //Arrange
            Warrior hero = new Warrior("Jeff");
            int expectedStrength = hero.heroAttributes.strength + 3;
            int expectedDexterity = hero.heroAttributes.dexterity + 2;
            int expectedIntelligence = hero.heroAttributes.intelligence + 1;
            //Act
            hero.LevelUp();
            int actualStrength = hero.heroAttributes.strength;
            int actualDexterity = hero.heroAttributes.dexterity;
            int actualIntelligence = hero.heroAttributes.intelligence;
            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);

        }

        //---------------- WEAPON ------------------------------------------------------------------
        [Fact]
        public void WeaponConstructor_InputsValidData_ShouldHaveCorrectName()
        {
            //Arrange
            Weapon weapon = new Weapon("Sting", 1, "Sword", 12);
            string expected = "Sting";
            //Act
            string actual = weapon.Name;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WeaponConstructor_InputsValidData_ShouldHaveCorrectRequiredLevel()
        {
            //Arrange
            Weapon weapon = new Weapon("Sting", 1, "Sword", 12);
            int expected = 1;
            //Act
            int actual = weapon.RequiredLevel;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WeaponConstructor_InputsValidData_ShouldHaveCorrectWeaponType()
        {
            //Arrange
            Weapon weapon = new Weapon("Sting", 1, "Sword", 12);
            string expected = "Sword";
            //Act
            string actual = weapon.WeaponType;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void WeaponConstructor_InputsValidData_ShouldHaveCorrectDamage()
        {
            //Arrange
            Weapon weapon = new Weapon("Sting", 1, "Sword", 12);
            int expected = 12;
            //Act
            int actual = weapon.WeaponDamage;
            //Assert
            Assert.Equal(expected, actual);
        }

        //---------------- ARMOR ------------------------------------------------------------------
        [Fact]
        public void ArmorConstructor_InputsValidData_ShouldHaveCorrectName()
        {
            //Arrange
            Armor armor = new Armor("Mithril Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            string expected = "Mithril Chainmail";
            //Act
            string actual = armor.Name;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void ArmorConstructor_InputsValidData_ShouldHaveCorrectRequiredLevel()
        {
            //Arrange
            Armor armor = new Armor("Mithril Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            int expected = 1;
            //Act
            int actual = armor.RequiredLevel;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void ArmorConstructor_InputsValidData_ShouldHaveCorrectSlotType()
        {
            //Arrange
            Armor armor = new Armor("Mithril Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            RPG_Heroes.Utils.Slot expected = RPG_Heroes.Utils.Slot.Body;
            //Act
            RPG_Heroes.Utils.Slot actual = armor.slot;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void ArmorConstructor_InputsValidData_ShouldHaveCorrectArmorType()
        {
            //Arrange
            Armor armor = new Armor("Mithril Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            string expected = "Mail";
            //Act
            string actual = armor.ArmorType;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void ArmorConstructor_InputsValidData_ShouldHaveCorrectarmorAttributes()
        {
            //Arrange
            Armor armor = new Armor("Mithril Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            int expectedStrength = 10;
            int expectedDexterity = 4;
            int expectedIntelligence = 2;
            //Act
            int actualStrength = armor.ArmorAttributes.strength;
            int actualDexterity = armor.ArmorAttributes.dexterity;
            int actualIntelligence = armor.ArmorAttributes.intelligence;

            //Assert
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        //---------------- Equip ------------------------------------------------------------------

        [Fact]
        public void EquipWeapon_InputsValidWeapon_ShouldBeEquipped()
        {
            //Arrange
            Warrior hero = new Warrior("Jon");
            Weapon weapon = new Weapon("Sting", 1, "Sword", 12);
            Weapon expected = weapon;
            //Act
            hero.Equip(weapon);
            var actual = hero.Equipment[RPG_Heroes.Utils.Slot.Weapon];
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipWeapon_InputsWeaponWithTooHighLevel_ShouldThrowError()
        {
            //Arrange
            Warrior hero = new Warrior("Jon");
            Weapon weapon = new Weapon("Sting", 10, "Sword", 12);
            Weapon expected = weapon;
            //Act Assert
            Assert.Throws<InvalidWeaponException>(() => hero.Equip(weapon));
        }

        [Fact]
        public void EquipWeapon_InputsWeaponWithWrongType_ShouldThrowError()
        {
            //Arrange
            Mage hero = new Mage("Jon");
            Weapon weapon = new Weapon("Sting", 10, "Sword", 12);
            Weapon expected = weapon;
            //Act Assert
            Assert.Throws<InvalidWeaponException>(() => hero.Equip(weapon));
        }

        [Fact]
        public void EquipArmor_InputsValidArmor_ShouldBeEquipped()
        {
            //Arrange
            Warrior hero = new Warrior("Jon");
            Armor armor = new Armor("Mithril Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            Armor expected = armor;

            //Act
            hero.Equip(armor);
            var actual = hero.Equipment[RPG_Heroes.Utils.Slot.Body];
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmor_InputsArmorWithTooHighLevel_ShouldThrowError()
        {
            //Arrange
            Warrior hero = new Warrior("Jon");
            Armor armor = new Armor("Mithril Chainmail", 5, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            //Act Assert
            Assert.Throws<InvalidArmorException>(() => hero.Equip(armor));
        }

        [Fact]
        public void EquipArmor_InputsArmorWithWrongType_ShouldThrowError()
        {
            //Arrange
            Mage hero = new Mage("Jon");
            Armor armor = new Armor("Mithril Chainmail", 5, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            //Act Assert
            Assert.Throws<InvalidArmorException>(() => hero.Equip(armor));
        }

        //---------------- TOTALATTRIBUTES ------------------------------------------------------------------

        [Fact]
        public void TotalAttributes_NoEquipment_ShouldreturnHeroAttributesWithoutExtraLevels()
        {
            //Arrange
            Mage hero = new Mage("Jon");
            HeroAttributes expected = hero.heroAttributes;
            //Act
            HeroAttributes actual = hero.TotalAttributes();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TotalAttributes_OnePieceOfArmor_ShouldreturnHeroAttributesWithArmorLevelsAdded()
        {
            //Arrange
            Warrior hero = new Warrior("Jon");
            Armor armor = new Armor("Mithril Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            HeroAttributes expected = new HeroAttributes(
                hero.heroAttributes.strength + 10,
                hero.heroAttributes.dexterity + 4,
                hero.heroAttributes.intelligence + 2
                );
            //Act
            hero.Equip(armor);
            HeroAttributes actual = hero.TotalAttributes();
            //Assert
            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        public void TotalAttributes_TwoPiecesOfArmor_ShouldreturnHeroAttributesWithArmorLevelsAdded()
        {
            //Arrange
            Warrior hero = new Warrior("Jon");
            Armor body = new Armor("Mithril Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            Armor helm = new Armor("Mithril Helmet", 1, RPG_Heroes.Utils.Slot.Head, "Mail", new HeroAttributes(4, 2, 1));
            HeroAttributes expected = new HeroAttributes(
                hero.heroAttributes.strength + 10 + 4,
                hero.heroAttributes.dexterity + 4 + 2,
                hero.heroAttributes.intelligence + 2 + 1
                );
            //Act
            hero.Equip(body);
            hero.Equip(helm);
            HeroAttributes actual = hero.TotalAttributes();
            //Assert
            expected.Should().BeEquivalentTo(actual);
        }

        [Fact]
        public void TotalAttributes_TwoArmorPiecesOfSameSlotType_ShouldreturnHeroAttributesWithLastEquipedBonusesOnly()
        {
            //Arrange
            Warrior hero = new Warrior("Jon");
            Armor armor1 = new Armor("Bronze Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(4, 1, 1));
            Armor armor2 = new Armor("Mithril Chainmail", 1, RPG_Heroes.Utils.Slot.Body, "Mail", new HeroAttributes(10, 4, 2));
            HeroAttributes expected = new HeroAttributes(
                hero.heroAttributes.strength + 10,
                hero.heroAttributes.dexterity + 4,
                hero.heroAttributes.intelligence + 2
                );
            //Act
            hero.Equip(armor1);
            hero.Equip(armor2);
            HeroAttributes actual = hero.TotalAttributes();
            //Assert
            expected.Should().BeEquivalentTo(actual);
        }

        //---------------- DAMAGE ------------------------------------------------------------------

        [Fact] 
        public void Damage_NoWeapon_ShouldReturnLowDamage()
        {
            //Arrange
            Mage hero = new Mage("Jon");
            int noWeaponDamage = 1;
            double expected = noWeaponDamage * (1 + (double)hero.heroAttributes.intelligence / 100);
            //Act
            double actual = hero.Damage();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_WeaponEquipped_ShouldReturnHighDamage()
        {
            //Arrange
            Mage hero = new Mage("Jon");
            Weapon staff = new Weapon("Staff of Testing", 1, "Staff", 8);
            int weaponDamage = 8;
            double expected = weaponDamage * (1 + (double)hero.heroAttributes.intelligence / 100);
            //Act
            hero.Equip(staff);
            double actual = hero.Damage();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Damage_ReplacesWeaponEquipped_ShouldReturnHighDamage()
        {
            //Arrange
            Mage hero = new Mage("Jon");
            Weapon wand = new Weapon("Wand of Testing", 1, "Wand", 4);
            Weapon staff = new Weapon("Staff of Testing", 1, "Staff", 8);
            int weaponDamage = 8;
            double expected = weaponDamage * (1 + (double)hero.heroAttributes.intelligence / 100);
            //Act
            hero.Equip(wand);
            hero.Equip(staff);
            double actual = hero.Damage();
            //Assert
            Assert.Equal(expected, actual);
        }
        /*
        [Fact]
        public void Damage_WeaponAndArmorEquipped_ShouldReturnHighDamage()
        {
            //Arrange
            Mage hero = new Mage("Jon");
            Armor robe = new Armor("Robe of Intellect", 1, RPG_Heroes.Utils.Slot.Body, "Cloth", new HeroAttributes(1,1,20));
            Weapon staff = new Weapon("Staff of Testing", 1, "Staff", 8);
            int weaponDamage = 8;
            int armorBonus = 20;
            double expected = weaponDamage * (1 + (double)(hero.heroAttributes.intelligence + armorBonus) / 100);
            //Act
            hero.Equip(robe);
            hero.Equip(staff);
            double actual = hero.Damage();
            //Assert
            Assert.Equal(expected, actual);
        }
        */
        //---------------- DISPLAY ------------------------------------------------------------------
        [Fact]
        public void Display_CheckDisplayOfHero_ShouldReturnStrinbuilderOfHeroInformation()
        {
            //Arrange
            StringBuilder expected = new StringBuilder();
            expected.AppendLine("-------------------------------");
            expected.AppendLine("Name:\t\t\t" + "Gandalf");
            expected.AppendLine("Class:\t\t\t" + "Mage");
            expected.AppendLine("Level:\t\t\t" + "1");
            expected.AppendLine("Total strength:\t\t" + "1");
            expected.AppendLine("Total dexterity:\t" + "1");
            expected.AppendLine("Total intelligence:\t" + "8");
            expected.AppendLine("Damage:\t\t\t" + "1.08");
            expected.AppendLine("-------------------------------");

            Mage hero = new Mage("Gandalf");
            //Act
            StringBuilder actual = hero.Display();
            //Assert
            actual.Should().BeEquivalentTo(expected);
        }

    }
}